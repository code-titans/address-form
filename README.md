# Address Form Angular Coding Challenge

Clone the repository and run **index.html** on **live-server** to understand the webpage form structure and responsiveness. Click **"Continue to Payment"** button to trigger form field validations and styling.

Write Angular 9+ code on IDE of your choosing - [StackBlitz](https://stackblitz.com) or [Visual Studio Code](https://code.visualstudio.com/) to meet below requirements:

## Requirements

1. Create new Angular project to deliver the responsive form. Drop **"form-validation.js"** file in new project.
2. Using Angular Reactive Forms, remove the field specific validations from html fields and move it to angular component so validation happens reactively as user types. **Hint:** use class **".is-invalid"** on 'input', 'select' for invalid styling.
3. The button **"Continue to Payment"** must be disabled until all fields in form are properly validated.
4. Click of enabled button **"Continue to Payment"** should print form data to browser console.
5. Code must be properly modularized, have unit tests and handle errors gracefully.
